require("./bootstrap");
import Vue from "vue";

import App from "../components/app.vue";

Vue.component("app-component", App);

const app = new Vue({
    el: "#app"
});
