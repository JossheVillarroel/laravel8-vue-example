# Run

## install composer dependencies

```bash
composer install
```

### install npm packages

```bash
npm i
```

### compile assets

```bash
npm run dev
```

### run

```bash
php artisan serve
```
